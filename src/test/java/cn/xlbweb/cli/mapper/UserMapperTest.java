package cn.xlbweb.cli.mapper;

import cn.xlbweb.cli.AppTests;
import cn.xlbweb.cli.model.User;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author: bobi
 * @date: 2019-09-15 13:29
 * @description:
 */
public class UserMapperTest extends AppTests {

    @Autowired
    private UserMapper userMapper;

    @Test
    public void countByExample() {
        long count = userMapper.countByExample(null);
        System.out.println(count);
    }

    @Test
    public void deleteByExample() {
    }

    @Test
    public void deleteByPrimaryKey() {
    }

    @Test
    public void insert() {
    }

    @Test
    public void insertSelective() {
    }

    @Test
    public void selectByExample() {
        List<User> userList = userMapper.selectByExample(null);
        System.out.println(userList);
    }

    @Test
    public void selectByPrimaryKey() {
        User user = userMapper.selectByPrimaryKey(1);
        System.out.println(user);
    }

    @Test
    public void updateByExampleSelective() {
    }

    @Test
    public void updateByExample() {
    }

    @Test
    public void updateByPrimaryKeySelective() {
    }

    @Test
    public void updateByPrimaryKey() {
    }
}