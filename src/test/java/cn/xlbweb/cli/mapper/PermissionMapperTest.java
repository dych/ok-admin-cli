package cn.xlbweb.cli.mapper;

import cn.xlbweb.cli.AppTests;
import cn.xlbweb.cli.model.Permission;
import cn.xlbweb.cli.util.CliUtils;
import cn.xlbweb.cli.vo.PermissionVo;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: bobi
 * @date: 2019-09-27 23:10
 * @description:
 */
public class PermissionMapperTest extends AppTests {

    @Autowired
    private PermissionMapper permissionMapper;

    @Test
    public void listPermission() {
//        PermissionExample permissionExample = new PermissionExample();
//        permissionExample.createCriteria().andTypeEqualTo(Const.PermissionType.DIRECTORY.getType());
        List<Permission> permissionList = permissionMapper.selectByExample(null);
        List<PermissionVo> permissionVoList = listPermissionTree(permissionList, 0);
        System.out.println(permissionVoList);
    }

    public List<PermissionVo> listPermissionTree(List<Permission> permissionList, Integer parentId) {
        List<PermissionVo> permissionVoList = new ArrayList<>();
        for (Permission permission : permissionList) {
            if (permission.getParentId() == parentId) {
                PermissionVo permissionVo = new PermissionVo();
                CliUtils.copyProperties(permission, permissionVo);
                permissionVoList.add(permissionVo);
            }
        }
        for (PermissionVo permissionVo : permissionVoList) {
            permissionVo.setChildren(listPermissionTree(permissionList, permissionVo.getId()));
        }
        return permissionVoList;
    }
}