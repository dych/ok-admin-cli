package cn.xlbweb.cli.util;

import org.junit.Test;

import java.util.UUID;

/**
 * @author: bobi
 * @date: 2019-09-16 23:01
 * @description:
 */
public class UUIDTest {

    @Test
    public void randomUUID() {
        for (int i = 0; i < 10; i++) {
            String id = UUID.randomUUID().toString();
            System.out.println(id);
        }
    }
}
