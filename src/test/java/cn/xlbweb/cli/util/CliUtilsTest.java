package cn.xlbweb.cli.util;

import cn.xlbweb.cli.model.User;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class CliUtilsTest {

    @Test
    public void copyProperties() {
        User user1 = new User();
        user1.setId(1);
        user1.setUsername("hello");
        user1.setPassword("1234546");

        User user2 = new User();
        user2.setPassword("111111");
//        BeanUtils.copyProperties(user1, user2);
        CliUtils.copyProperties(user1, user2);
        System.out.println(user2);
    }

    @Test
    public void listToSet() {
        List<String> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        list.add("c");
        list.add("a");
        list.add(null);
//        Set<String> set = new HashSet<>(list);
        Set<String> set = CliUtils.listToSet(list);
        System.out.println(set);
    }
}